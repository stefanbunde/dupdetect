import os
import pytest

from dupdetect.checksum import calculate_checksum
from dupdetect.file_index import FileIndex


PATH = "tests/data/green.txt"

@pytest.fixture
def index():
    return FileIndex()


def test_add_to_index(index):
    filesize = os.path.getsize(PATH)
    checksum = calculate_checksum(PATH)
    index._add_to_index(PATH, filesize)

    assert len(index._index) == 1
    assert index._index[(checksum, filesize)] == [PATH]


def test_index_file_without_min_filesize(index):
    index._index_file(PATH)
    assert len(index._index) == 1


def test_index_file_with_min_filesize(index):
    index.min_filesize = 10
    index._index_file(PATH)
    assert len(index._index) == 0


def test_create(index):
    index.create("tests/data")
    expected_keys = {('1098e2cb1442f45f8ca2e74e1cd24bd0', 4),
                     ('15e87a35c76d7469dd435c43d73bdbe8', 6),
                     ('4b5f940728b232b034e4e50555ba4046', 6),
                     ('5626cc7e05afb1629f320de336c1acd5', 6),
                     ('6aefd2842be62cd470709b27aedc7db7', 7),
                     ('b23c9cacdb9718fcc6ee0090c9c0e03d', 5),
                     ('daa5960a123ff55e594be19f9ddc940d', 5)}
    assert expected_keys == set(index._index.keys())


def test_get_duplicated_files(index):
    index.create("tests/data")
    expected = [('1098e2cb1442f45f8ca2e74e1cd24bd0', 4,
                 ['tests/data/subdir1_level1/subdir1_level2/red.txt',
                  'tests/data/subdir2_level1/red.txt']),
                ('4b5f940728b232b034e4e50555ba4046', 6,
                 ['tests/data/green.txt', 'tests/data/subdir1_level1/subdir1_level2/green.txt']),
                ('6aefd2842be62cd470709b27aedc7db7', 7,
                 ['tests/data/orange.txt', 'tests/data/subdir1_level1/subdir1_level2/orange.txt',
                  'tests/data/subdir2_level1/orange.txt']),
                ('daa5960a123ff55e594be19f9ddc940d', 5,
                 ['tests/data/blue.txt', 'tests/data/subdir1_level1/blue.txt'])]
    assert expected == sorted(list(index.get_duplicated_files()))


def test_get_duplicates_ordered_by_size(index):
    index.create("tests/data")
    expected = [('1098e2cb1442f45f8ca2e74e1cd24bd0', 4,
                 ['tests/data/subdir1_level1/subdir1_level2/red.txt',
                  'tests/data/subdir2_level1/red.txt']),
                ('daa5960a123ff55e594be19f9ddc940d', 5,
                 ['tests/data/blue.txt', 'tests/data/subdir1_level1/blue.txt']),
                ('4b5f940728b232b034e4e50555ba4046', 6,
                 ['tests/data/green.txt', 'tests/data/subdir1_level1/subdir1_level2/green.txt']),
                ('6aefd2842be62cd470709b27aedc7db7', 7,
                 ['tests/data/orange.txt', 'tests/data/subdir1_level1/subdir1_level2/orange.txt',
                  'tests/data/subdir2_level1/orange.txt'])]
    assert expected == index.get_duplicated_files_ordered_by_size()
