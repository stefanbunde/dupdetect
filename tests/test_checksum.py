import pytest

from dupdetect.checksum import calculate_checksum


@pytest.fixture
def testfile():
    return "tests/data/white.txt"


def test_calculate_checksum(testfile):
    checksum = calculate_checksum(testfile)
    assert "15e87a35c76d7469dd435c43d73bdbe8" == checksum
