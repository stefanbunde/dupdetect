from setuptools import find_packages, setup


setup(
    name="dupdetect",
    version="0.1.0",

    author="Stefan Bunde",
    author_email="misc.stefanbunde@gmail.com",

    packages=find_packages(),

    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
)
