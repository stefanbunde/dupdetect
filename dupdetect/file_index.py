import os
from collections import defaultdict

from dupdetect.checksum import calculate_checksum


class FileIndex(object):
    def __init__(self, min_filesize=0):
        self._index = defaultdict(list)
        self.min_filesize = min_filesize

    def create(self, root_directory):
        for dirpath, dnames, fnames in os.walk(root_directory):
            for fname in fnames:
                filepath = os.path.join(dirpath, fname)
                self._index_file(filepath)

    def _index_file(self, filepath):
        filesize = os.path.getsize(filepath)
        if filesize < self.min_filesize:
            return
        self._add_to_index(filepath, filesize)

    def _add_to_index(self, filepath, filesize):
        checksum = calculate_checksum(filepath)
        self._index[(checksum, filesize)].append(filepath)

    def get_duplicated_files(self):
        for (checksum, filesize), filenames in self._index.items():
            if len(filenames) > 1:
                yield checksum, filesize, filenames

    def get_duplicated_files_ordered_by_size(self):
        return sorted(list(self.get_duplicated_files()), key=lambda item: item[1])
