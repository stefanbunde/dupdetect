import hashlib


def calculate_checksum(path):
   content = read_file_content(path)
   return calculate_md5_checksum(content)


def read_file_content(filepath):
    with open(filepath, 'rb') as f:
        return f.read()


def calculate_md5_checksum(file_content):
    return hashlib.md5(file_content).hexdigest()
