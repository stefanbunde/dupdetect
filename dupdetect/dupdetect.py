import hashlib
import os
from optparse import OptionParser

from dupdetect.checksum import calculate_checksum
from dupdetect.file_index import FileIndex


class Config(object):
    verbose = False
    min_filesize = 0


def get_config(options):
    config = Config()
    if options.min_filesize:
        config.min_filesize = _convert_filesize(options.min_filesize)
    if options.verbose:
        config.verbose = options.verbose
    return config


def _convert_filesize(filesize_repr):
    if filesize_repr.isdigit():
        return int(filesize_repr)

    if not filesize_repr[-1] in "KMG" or not filesize_repr[:-1].isdigit():
        raise RuntimeError

    filesize = int(filesize_repr[:-1])
    if filesize_repr[-1] == "K":
        filesize *= 1000
    elif filesize_repr[-1] == "M":
        filesize *= 1000 * 1000
    elif filesize_repr[-1] == "G":
        filesize *= 1000 * 1000 * 1000

    return filesize


def parse_options_and_arguments():
    parser = OptionParser()
    parser.add_option("-m", "--min-filesize", dest="min_filesize")
    parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False)
    return parser.parse_args()


def check_if_path_exists(path):
    if not os.path.exists(path):
        print("Error: path does not exist \"{}\"".format(path))
        exit(0)


def main():
    options, args = parse_options_and_arguments()
    config = get_config(options)

    if len(args) != 1:
        parser.error("incorrect number of arguments")

    root_path = args[0]
    check_if_path_exists(root_path)

    file_index = FileIndex(config.min_filesize)
    file_index.create(root_path)

    for checksum, filesize, filenames in file_index.get_duplicated_files_ordered_by_size():
        print("{} - {}".format(checksum, filesize))
        for filename in filenames:
            print(filename)
        print()
